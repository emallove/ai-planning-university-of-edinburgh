#!/usr/bin/env perl
#
#

##################################################################
#
#
# File: aiplan-assignment1-question1.pl
#
#
##################################################################

use POSIX qw(ceil floor);
use Data::Dumper;
use TileGameState;

use Graph;
use Graph::Directed;
use Graph::Undirected;
my $graph_of_romania = Graph::Undirected->new;

# Unit tests
require "run-unit-tests.pl";

# Subroutines for managing the tile board game
#   (e.g., finding tile positions, modifying the board)
require "tilegame-helper-subs.pl";

# Global variables for tile game (e.g., board size)
require "tilegame-global-vars.pl";

use strict;

# Question 1
#
# Implement the A* algorithm and the Eight-Puzzle in a programming language of
# your choice.
#
# The most challenging part will probably be the queue of fringe nodes (ordered
# by f-value). For the Eight-Puzzle an array of 32 queues for the different
# possible f-values will do.
#
# (Q: Why 32?) A: Because each of the 8 tiles can branch to - at most -
#   four new states. 8 * 4 = 32
#
#
# A more general solution would be a priority queue implemented as a binary
# tree. Some programming languages have an appropriate data structure in their
# standard library.
#
# The state of the puzzle can be represented by a simple array of numbers that
# lists the tiles in the puzzle row by row. The number zero can be used for the
# empty position. For example, the goal state can be represented by the array
#
#   [0 1 2 3 4 5 6 7 8]
#
# Test your implementation with the initial state [ 1 6 4 8 7 0 3 2 5]. What is
# the optimal solution path length?
#
# Marks available: 2
#

# 0,1,2
# 3,4,5,
# 6,7,8
#
my $global_goal_state = "Bucharest";
my $global_start_state = "Arad";

# Homework goal.  30-step?
# $global_start_state = [
#   1, 6, 4,
#   8, 7, 0,
#   3, 2, 5];


# DEBUGGING - easier start state
# 1,0,2
# 3,4,5,
# 6,7,8

# 1-step solution
# $global_start_state  = [
#   0, 1, 2,
#   3, 4, 5,
#   6, 7, 8];

# 2-step solution
# $global_start_state  = [
#   1, 2, 0,
#   3, 4, 5,
#   6, 7, 8];

# 3-step solution
# $global_start_state  = [
#   1, 2, 5,
#   3, 4, 0,
#   6, 7, 8];

# 4-step solution
# $global_start_state  = [
#   1, 2, 5,
#   3, 4, 8,
#   6, 7, 0];
#

my $straight_line_dist_to_Bucharest = {
  "Arad"           =>  366,
  "Hirsova"        =>  151,
  "RimnicuVilcea"  =>  193,
  "Bucharest"      =>  0,
  "Iasi"           =>  226,
  "Craiova"        =>  160,
  "Lugoj"          =>  244,
  "Sibiu"          =>  253,
  "Dobreta"        =>  242,
  "Mehadia"        =>  241,
  "Timisoara"      =>  329,
  "Eforie"         =>  161,
  "Neamt"          =>  234,
  "Urziceni"       =>  80,
  "Fagaras"        =>  176,
  "Oradea"         =>  380,
  "Vaslui"         =>  199,
  "Giurgiu"        =>  77,
  "Pitesti"        =>  100,
  "Zerind"         =>  374,
};

$graph_of_romania->add_vertex("Arad");
$graph_of_romania->add_vertex("Hirsova");
$graph_of_romania->add_vertex("RimnicuVilcea");
$graph_of_romania->add_vertex("Bucharest");
$graph_of_romania->add_vertex("Iasi");
$graph_of_romania->add_vertex("Craiova");
$graph_of_romania->add_vertex("Lugoj");
$graph_of_romania->add_vertex("Sibiu");
$graph_of_romania->add_vertex("Dobreta");
$graph_of_romania->add_vertex("Mehadia");
$graph_of_romania->add_vertex("Timisoara");
$graph_of_romania->add_vertex("Eforie");
$graph_of_romania->add_vertex("Neamt");
$graph_of_romania->add_vertex("Urziceni");
$graph_of_romania->add_vertex("Fagaras");
$graph_of_romania->add_vertex("Oradea");
$graph_of_romania->add_vertex("Vaslui");
$graph_of_romania->add_vertex("Giurgiu");
$graph_of_romania->add_vertex("Pitesti");
$graph_of_romania->add_vertex("Zerind");

$graph_of_romania->add_weighted_edge("Oradea",         "Zerind",         71);
$graph_of_romania->add_weighted_edge("Zerind",         "Arad",           75);
$graph_of_romania->add_weighted_edge("Arad",           "Timisoara",      118);
$graph_of_romania->add_weighted_edge("Arad",           "Sibiu",          140);
$graph_of_romania->add_weighted_edge("Timisoara",      "Lugoj",          111);
$graph_of_romania->add_weighted_edge("Lugoj",          "Mehadia",        70);
$graph_of_romania->add_weighted_edge("Mehadia",        "Dobreta",        75);
$graph_of_romania->add_weighted_edge("Dobreta",        "Craiova",        120);
$graph_of_romania->add_weighted_edge("Oradea",         "Sibiu",          151);
$graph_of_romania->add_weighted_edge("Sibiu",          "RimnicuVilcea",  80);
$graph_of_romania->add_weighted_edge("Sibiu",          "Fagaras",        99);
$graph_of_romania->add_weighted_edge("RimnicuVilcea",  "Craiova",        146);
$graph_of_romania->add_weighted_edge("RimnicuVilcea",  "Pitesti",        97);
$graph_of_romania->add_weighted_edge("Craiova",        "Pitesti",        138);
$graph_of_romania->add_weighted_edge("Pitesti",        "Bucharest",      101);
$graph_of_romania->add_weighted_edge("Bucharest",      "Giurgiu",        90);
$graph_of_romania->add_weighted_edge("Bucharest",      "Fagaras",        211);
$graph_of_romania->add_weighted_edge("Bucharest",      "Urziceni",       85);
$graph_of_romania->add_weighted_edge("Urziceni",       "Vaslui",         142);
$graph_of_romania->add_weighted_edge("Urziceni",       "Hirsova",        98);
$graph_of_romania->add_weighted_edge("Hirsova",        "Eforie",         86);
$graph_of_romania->add_weighted_edge("Vaslui",         "Iasi",           92);
$graph_of_romania->add_weighted_edge("Iasi",           "Neamt",          87);


##################################################################
#
#
#
# Heuristics for search algorithm
#
#
#
##################################################################

# Number of misplaced tiles
#
#   h(n) in our cost function defined as
#     f(n) = h(n) + g(n)
sub heuristic1 {
  my ($curr_state, $goal_state) = @_;

  my $misplacement_count = 0;

  for (my $i = 0; $i < @$curr_state; $i++) {
    if ($curr_state->[$i] != $goal_state->[$i]) {
      $misplacement_count++;
    }
  }

  return $misplacement_count;
}

# Sum of each tile's Manhattan-distance to goal
#
#   g(n) in our cost function defined as
#     f(n) = h(n) + g(n)
sub heuristic2 {
  my ($curr_state, $goal_state) = @_;

  my $sum_of_manhattan_distances = 0;

  for (my $i = 0; $i < @$curr_state; $i++) {
    my $find_this = $curr_state->[$i];

    my $found_pos = index_of($find_this, $goal_state);

    if ($found_pos != -1) {
      $sum_of_manhattan_distances += manhattan_distance($i, $found_pos);
    } else {
      die "Couldn't find term $find_this\n";
    }
  }

  return $sum_of_manhattan_distances;
}

##################################################################
#
#
#
# Implementation of A star algorithm
#
#
#
##################################################################

#
# Source: https://en.wikipedia.org/wiki/A*_search_algorithm#Pseudocode
#

# FIXME: In the below [Perl-ified] pseudocode, are the following CLASSES or INSTANCES?
#
#   start
#   neighbor
#   goal
#   came_from
#   current
#
#

# Global table of f-score's and g-score's
my $f_score;
my $g_score;

my $tree_of_expanded_nodes = Graph::Undirected->new;

use Carp;
$SIG{ __DIE__ } = sub { Carp::confess( @_ ) };


# Store the path 
#
# TODO: Do this with a more lightweight structure (e.g., list), instead of a heavy 
# undirected graph?
my $path_to_destination = Graph::Directed->new;

# Hash to record distances from our start node, so that 
# we can reconstruct the path based on our search tree,
# and not our open queue.
my $distance_from_start;

sub AStarSearch {
  my ($start, $goal) = @_;

    # The set of nodes already evaluated
    my $closedset;

    # The set of tentative nodes to be evaluated, initially containing the start node
    my $openset;
    $openset->{keyify($start)} = $start;

    # The map of navigated nodes
    my $came_from;
    $came_from->{'s' . keyify($start)} = undef;

    # Cost from start along best known path
    # Q: why is this initialized to 0?  Why not heuristic_cost_estimate()
    # A: The cost from start to start is 0
    $g_score->{keyify($goal)} = 0;

    # Estimated total cost from start to goal through y
    #
    $f_score->{keyify($start)} = $g_score->{keyify($start)} + heuristic_cost_estimate($start, $goal);

    my $depth;

    # FIXME: seems odd, but how else do we initialize without breaking Perl's "strict" mode?
    my $previous = "";

    while ((keys %$openset) > 0) {

        # The node in openset having the lowest f_score[ value
        #
        # Structure needed to _BACKTRACE_ from goal to start
        #   came_from -> leaf    = parent1
        #   came_from -> parent1 = parent2
        #   came_from -> parent2 = parent3
        #   came_from -> start   = undef

        # How do we know when we've backed out of a depth-first dive?
        # Can we track the distance from the start?
        my $current = get_best_f_score($openset);

        if ($current ~~ $goal) {
            print 'line 372 EAM reconstruct_path($start, $goal) = ' . Dumper(reconstruct_path($start, $goal)) . "\n";
            exit;
        }

        # Build path from goal to start
        my @s = $path_to_destination->successors($previous);
        # foreach my $s (@s) {
        #   $path_to_destination->delete_edge($previous, $s);
        # }

        if ($graph_of_romania->has_edge($previous, $current)) {
          $path_to_destination->add_edge($previous, $current);
        }

        # remove current from openset
        delete($openset->{keyify($current)});

        # Add current to closedset
        $closedset->{keyify($current)} = $current;

        # Dummy value for Poor Man's scanf debugger
        my $d;

        foreach my $neighbor (@{neighbor_nodes($current)}) {

            my $tentative_g_score = $g_score->{keyify($current)} + dist_between($current, $neighbor);

            if ((! defined($openset->{keyify($neighbor)})) or ($tentative_g_score < $g_score->{keyify($neighbor)})) {

                $g_score->{keyify($neighbor)} = $tentative_g_score;
                $f_score->{keyify($neighbor)} = $g_score->{keyify($neighbor)} + heuristic_cost_estimate($neighbor, $goal);

                if (! defined($openset->{keyify($neighbor)})) {
                    $openset->{keyify($neighbor)} = $neighbor;
                }
            }

            # Delete all edges, except the neighbor
            my @s = $path_to_destination->successors($current);
            foreach my $s (@s) {
              $path_to_destination->delete_edge($current, $s);
            }
            if ($graph_of_romania->has_edge($current, $neighbor)) {
              $path_to_destination->add_edge($current, $neighbor);
            }
        }

        $previous = $current;
    }

    reconstruct_path($start, $goal);

    # walk backwards from goal to start
    my @p;
    my $v = $goal;
    while (@p = $path_to_destination->predecessors($v)) {
      $v = $p[0];
      if ($p[0] eq $start) {
        return;
      }
    }


    # Failed to find a path.  Can't catch a loop in this algorithm?
    return my $failure = undef;
}

sub keyify {
  my $r = shift;
  return $r;
}

sub get_best_f_score {
  my ($openset) = @_;

  my $best_f_score_key;
  my $best_f_score_val = 2**32;
  foreach my $k (keys %$openset) {
    if ($f_score->{keyify($openset->{$k})} < $best_f_score_val) {
      $best_f_score_val = $f_score->{$k};
      $best_f_score_key = $k;
    }
  }
  return $openset->{$best_f_score_key};
}

# Keep the symbol name (neighbor_nodes) from the Wikipedia pseudocode example
sub neighbor_nodes {
  my ($v) = @_;
  my @r = $graph_of_romania->successors($v);
  return \@r;
}

# Neighbors are always 1-step away in our toy tile-puzzle
sub dist_between {
  my ($node1, $node2) = @_;
  return $graph_of_romania->get_edge_weight($node1, $node2);
}

# Keep the symbol name (heuristic_cost_estimate) from the Wikipedia pseudocode example
sub heuristic_cost_estimate {
  return $straight_line_dist_to_Bucharest->{$_[0]};
}

# Keep the symbol name (reconstruct_path) from the Wikipedia pseudocode example
# FIXME: how does this sub work?
sub reconstruct_path {
  my ($start, $goal) = @_;

  my $current = $start;

  my @path_from_goal_to_start;

  push(@path_from_goal_to_start, $start);

  while ($current ne $goal) {

    my $min_dist_val = 2**32;
    my $min_dist_key = $goal;
    my $weighted_edge;

    foreach my $s ($path_to_destination->successors($current)) {

      $weighted_edge = $graph_of_romania->get_edge_weight($s, $current);
      if ($weighted_edge < $min_dist_val) {

        # TODO: FINISH THIS!
        $min_dist_val = $weighted_edge;
        $min_dist_key = $s;
      }
    }

    $current = $min_dist_key;

    push(@path_from_goal_to_start, $min_dist_key);
  }

  return \@path_from_goal_to_start;
}


# Enumerate fringe states to a depth of n
#
#   . . .
#   . o .
#   . . .
#
#

sub get_fringe_states {
  my ($state) = @_;

  my $fringe = ();

  my $empty_pos = index_of(0, $state);

  # The possible states are moving the open square
  # to a vertically or horizontally adjacent square.
  # Moving open square means swapping the open square
  # with an adjacent occupied square.

  # Calculate the rows and columns
  my ($empty_pos_row, $empty_pos_col) = @{arrayIndex_to_cartesianCoordinates($empty_pos)};

  # Swap empty position with each adjacent tile-occupied positions (if it exists)

  # left
  my $left_pos = cartesianCoordinates_to_arrayIndex($empty_pos_row, $empty_pos_col - 1);

  my $i = 0;

  if (defined($left_pos)) {
    my $new_state1 = swap($state, $empty_pos, $left_pos);
    push(@$fringe, $new_state1);
  }

  # right
  my $right_pos = cartesianCoordinates_to_arrayIndex($empty_pos_row, $empty_pos_col + 1);
  if (defined($right_pos)) {
    my $new_state2 = swap($state, $empty_pos, $right_pos);
    push(@$fringe, $new_state2);
  }

  # above
  my $above_pos = cartesianCoordinates_to_arrayIndex($empty_pos_row - 1, $empty_pos_col);
  if (defined($above_pos)) {
    my $new_state3 = swap($state, $empty_pos, $above_pos);
    push(@$fringe, $new_state3);
  }

  # below
  my $below_pos = cartesianCoordinates_to_arrayIndex($empty_pos_row + 1, $empty_pos_col);
  if (defined($below_pos)) {
    my $new_state4 = swap($state, $empty_pos, $below_pos);
    push(@$fringe, $new_state4);
  }

  return $fringe;
}

my $global_fringe_depth = 2;

# Variant on the above subroutine, but which adds a depth parameter
# to get a _DEEPER_ fringe into the game state tree
sub get_fringe_states2 {
  my ($state, $depth) = @_;

  # If we're at zero depth, we're done.  We can return now.
  if ($depth == 0) {
    return;
  }

  my $fringe = ();

  my $empty_pos = index_of(0, $state);

  # The possible states are moving the open square
  # to a vertically or horizontally adjacent square.
  # Moving open square means swapping the open square
  # with an adjacent occupied square.

  # Calculate the rows and columns
  my ($empty_pos_row, $empty_pos_col) = @{arrayIndex_to_cartesianCoordinates($empty_pos)};

  # Swap empty position with each adjacent tile-occupied positions (if it exists)

  # left
  my $left_pos = cartesianCoordinates_to_arrayIndex($empty_pos_row, $empty_pos_col - 1);

  my $i = 0;

  if (defined($left_pos)) {
    my $new_state1 = swap($state, $empty_pos, $left_pos);
    push(@$fringe, $new_state1);
    my $deeper_fringe1 = get_fringe_states2($new_state1, $depth - 1);
    if (defined($deeper_fringe1)) {
      push(@$fringe, $deeper_fringe1);
    }
  }

  # right
  my $right_pos = cartesianCoordinates_to_arrayIndex($empty_pos_row, $empty_pos_col + 1);
  if (defined($right_pos)) {
    my $new_state2 = swap($state, $empty_pos, $right_pos);
    push(@$fringe, $new_state2);
    my $deeper_fringe2 = get_fringe_states2($new_state2, $depth - 1);
    if (defined($deeper_fringe2)) {
      push(@$fringe, $deeper_fringe2);
    }
  }

  # above
  my $above_pos = cartesianCoordinates_to_arrayIndex($empty_pos_row - 1, $empty_pos_col);
  if (defined($above_pos)) {
    my $new_state3 = swap($state, $empty_pos, $above_pos);
    push(@$fringe, $new_state3);
    my $deeper_fringe3 = get_fringe_states2($new_state3, $depth - 1);
    if (defined($deeper_fringe3)) {
      push(@$fringe, $deeper_fringe3);
    }
  }

  # below
  my $below_pos = cartesianCoordinates_to_arrayIndex($empty_pos_row + 1, $empty_pos_col);
  if (defined($below_pos)) {
    my $new_state4 = swap($state, $empty_pos, $below_pos);
    push(@$fringe, $new_state4);
    my $deeper_fringe4 = get_fringe_states2($new_state4, $depth - 1);
    if (defined($deeper_fringe4)) {
      push(@$fringe, $deeper_fringe4);
    }
  }

  return $fringe;
}

# Outer get_fringe_states routine to kick-off the recursion
sub get_fringe_states_starter {
  return get_fringe_states2(@_);
}


##################################################################
#
#
#
# Main program
#
#
#
##################################################################

run_unit_tests();
# exit;

&AStarSearch($global_start_state, $global_goal_state);
exit;

# DEBUGGING
print "DEBUG: get_fringe_states() ---\n";
foreach my $s (get_fringe_states($global_start_state)) {
  dump_tile_game_state($s);
  print "---\n";
}

#
# vim @d register for debugging perl vars
# vim: let @d = yoprint ' EAM ' . pa . "\n";0wwwwPa = 
#
# vim @l register for adding a line number to a trace
# vim let @l = 0f'aline :let @n = line('.')"np
#
# Combine the two :-)
# vim let @d .= @l
