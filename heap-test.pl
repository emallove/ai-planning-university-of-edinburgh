#!/usr/bin/env perl


use Heap::Binary;

# use TileGameState;

use myObject;

my $heap = Heap::Binary->new;
my $elem;

my $dummy_game_state = [4,6,5,2,3,1,8,7,9];

foreach $i ( 1..100 ) {

    $tgs = myObject->new($i);

    # $tgs->{f_value} = $i;
    # $tgs->{game_state} = $dummy_game_state;

    $heap->add( $tgs );
}

while( defined( $elem = $heap->extract_top ) ) {
    print "Smallest is ", $elem, "\n";
}
