#!/usr/bin/env perl
#

use strict;
use Tree::Nary;

my $node = new Tree::Nary;
my $another_node = new Tree::Nary;


my $parent = 100;
my $position = 0;

my $inserted_node;
$inserted_node = $node->insert($parent, $position, $node);
$parent++;
$inserted_node = $node->insert_before($parent, $sibling, $node);
$parent++;
$inserted_node = $node->append($parent, $node);
$parent++;
$inserted_node = $node->prepend($parent, $node);
$parent++;
$inserted_node = $node->insert_data($parent, $position, $data);
$parent++;
$inserted_node = $node->insert_data_before($parent, $sibling, $data);
$parent++;
$inserted_node = $node->append_data($parent, $data);
$parent++;
$inserted_node = $node->prepend_data($parent, $data);
$parent++;

$node->reverse_children($node);

$node->traverse($node, $order, $flags, $maxdepth, $funcref, $argref);

$node->children_foreach($node, $flags, $funcref, $argref);

$root_node = $obj->get_root($node);

$found_node = $node->find($node, $order, $flags, $data);
$found_child_node = $node->find_child($node, $flags, $data);

$index = $node->child_index($node, $data);
$position = $node->child_position($node, $child);

$first_child_node = $node->first_child($node);
$last_child_node = $node->last_child($node);

$nth_child_node = $node->nth_child($node, $index);

$first_sibling = $node->first_sibling($node);
$next_sibling = $node->next_sibling($node);
$prev_sibling = $node->prev_sibling($node);
$last_sibling = $node->last_sibling($node);

$bool = $node->is_leaf($node);
$bool = $node->is_root($node);

$cnt = $node->depth($node);

$cnt = $node->n_nodes($node);
$cnt = $node->n_children($node);

$bool = $node->is_ancestor($node);

$cnt = $obj->max_height($node);

$node->tsort($node);

$normalized_node = $node->normalize($node);

$bool = $node->is_identical($node, $another_node);
$bool = $node->has_same_struct($node, $another_node);

$node->unlink($node);
