##################################################################
#
#
# File: tilegame-helper-subs.pl
#
#
##################################################################

use strict;

our $length_horizontal = 3;
our $length_vertical = 3;

# Swap the values at the two given positions
sub swap {
  my ($arr, $p1, $p2) = @_;

  if ((! defined($p1)) or (! defined($p2))) {
    print "Will not swap an undefined value.  Are you off the ${length_horizontal}x${length_vertical}-tile board?\n";
    return undef;
  }

  if (($p1 < 0) or ($p2 < 0) or ($p1 >= @$arr) or ($p2 >= @$arr)) {
    # print "Cannot swap values into a negative index: ($p1, $p2)\n";
    return undef;
  }

  # Do not mutate the argument, return a modified copy
  my @ret = @$arr;

  my $tmp = $ret[$p1];
  $ret[$p1] = $ret[$p2];
  $ret[$p2] = $tmp;

  return \@ret;
}

# Cartesian coordinates are in (row, column) format,
# array indicies are simply an integer indexes.
# Our NxN tile game is stored as a simple array, but
# is conceptually a 2D grid.
sub cartesianCoordinates_to_arrayIndex {
  my ($row, $col) = @_;

  if ((($row < 0) or 
       ($col < 0)) or 
       ($row >= $length_vertical) or 
       ($col >= $length_horizontal)) {
    # print "The supplied cartesian coordinates go off the ${length_horizontal}x${length_vertical}-tile board: ($row, $col)\n";
    return undef;
  }

  return ($row * $length_horizontal) + $col;
}

# Inverse function of cartesianCoordinates_to_arrayIndex()
#
# Cartesian coordinates are in (row, column) format,
# array indicies are simply an integer indexes.
# Our NxN tile game is stored as a simple array, but
# is conceptually a 2D grid.
#
# Accepts: integer array index
# Returns: (row, column) pair in the NxN grid
#
sub arrayIndex_to_cartesianCoordinates {
  my ($index) = @_;

  my $column = $index % $length_vertical;
  my $row = floor($index / $length_horizontal);
  return [$row, $column];
}

##################################################################
#
#
#
# Helper functions
#
#
#
##################################################################

sub index_of {
  my ($int, $arr) = @_;

  for (my $i = 0; $i < @$arr; $i++) {
    if ($arr->[$i] == $int) {
      return $i;
    }
  }

  # -1 is search term "Not found", just like Perl's built-in index() subroutine:
  #   http://perldoc.perl.org/functions/
  return -1;
}

sub compare_arrays_of_game_states {
  my ($arr1, $arr2) = @_;

  my $hash1;
  my $hash2;

  foreach my $e (@$arr1) {
    $hash1->{ join(",", @$e) };
  }
  foreach my $e (@$arr2) {
    $hash2->{ join(",", @$e) };
  }

  foreach my $k (keys %$hash1) {
    if (! exists($hash2->{$k})) {
      print "$k is in arr1, but not in arr2\n";
      return undef;
    } else {
      print "Hooray!  Game state [$k] is in arr1 and arr2\n";
    }
  }
  foreach my $k (keys %$hash2) {
    if (! exists($hash1->{$k})) {
      print "$k is in arr2, but not in arr1\n";
      return undef;
    } else {
      print "Hooray!  Game state [$k] is in arr1 and arr2\n";
    }
  }

  # No mismatches - the arrays are essentially identical
  return 1;
}

sub manhattan_distance {
  my ($curr_pos, $goal_pos) = @_;

  # Calculate the rows and columns
  my ($curr_col, $curr_row) = @{arrayIndex_to_cartesianCoordinates($curr_pos)};
  my ($goal_col, $goal_row) = @{arrayIndex_to_cartesianCoordinates($goal_pos)};

  # Manhattan distance is the delta columns + delta rows
  return abs($curr_col - $goal_col) + abs($curr_row - $goal_row);
}

sub dump_tile_game_state {
  my ($state) = @_;

  my $ret = "";

  for (my $i = 0; $i < @$state; $i++) {
    if ($i % 3 == 0) {
      $ret .= "\n";
    }
    $ret .= $state->[$i] . " ";
  }

  return $ret;
}

1;
