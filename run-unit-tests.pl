##################################################################
#
#
# File: run-unit-tests.pl
#
#
##################################################################


use strict;

my $length_horizontal = 3;
my $length_vertical = 3;

##################################################################
#
#
#
# Unit tests
#
#
#
##################################################################
#

sub run_unit_tests {

  # swap()
  my $t_arr1 = swap([1,2,3], 0, 2);

  my $column_width = "25.55";
  my $dot_multiplier = 100;

  # ~~ is the new SmartMatch operator in Perl 5.10!
  #   http://perldoc.perl.org/perlop.html#Smartmatch-Operator
  #
  my $t_arr2 = [3,2,1];
  if (@$t_arr1 ~~ @$t_arr2) {
    print( sprintf("%-${column_width}s", "swap()" . ("." x $dot_multiplier)) . 
      "TEST PASSED: Expected [" . join(",", @$t_arr2) . "], and got [" . join(",", @$t_arr1) . "].\n");
  } else {
    warn( sprintf("%-${column_width}s", "swap()" . ("." x $dot_multiplier)) . 
      "TEST FAILED: Expected [" . join(",", @$t_arr2) . "], but got [" . join(",", @$t_arr1) . "].\n");
  }

  # index_of()
  my $t_index1 = index_of(66, [0,1,2,3,4,5,66,7]);
  my $t_index2 = 6;
  if ($t_index1 == 6) {
    print( sprintf("%-${column_width}s", "index_of()" . ("." x $dot_multiplier)) . 
      "TEST PASSED: Expected $t_index2, and got $t_index1.\n");
  } else {
    warn( sprintf("%-${column_width}s", "index_of()" . ("." x $dot_multiplier)) . 
      "TEST FAILED: Expected $t_index2, but got $t_index1.\n");
  }

  # arrayIndex_to_cartesianCoordinates()
  my $t_cartesian1 = arrayIndex_to_cartesianCoordinates(7);
  my $t_cartesian2 = [2,1];
  if (@$t_cartesian1 ~~ @$t_cartesian2) {
    print( sprintf("%-${column_width}s", "arrayIndex_to_cartesianCoordinates()" . ("." x $dot_multiplier)) . 
      "TEST PASSED: Expected (" . join(",", @$t_cartesian1) . "), and got (" . join(",", @$t_cartesian2) . ").\n");
  } else {
    print( sprintf("%-${column_width}s", "arrayIndex_to_cartesianCoordinates()" . ("." x $dot_multiplier)) . 
      "TEST FAILED: Expected (" . join(",", @$t_cartesian1) . "), but got (" . join(",", @$t_cartesian2) . ").\n");
  }

  # cartesianCoordinates_to_arrayIndex()
  $t_cartesian1 = cartesianCoordinates_to_arrayIndex(1,2);
  $t_cartesian2 = 5;
  if ($t_cartesian1 == $t_cartesian2) {
    print( sprintf("%-${column_width}s", "cartesianCoordinates_to_arrayIndex()" . ("." x $dot_multiplier)) . 
      "TEST PASSED: Expected $t_cartesian2, and got $t_cartesian1.\n");
  } else {
    print( sprintf("%-${column_width}s", "cartesianCoordinates_to_arrayIndex()" . ("." x $dot_multiplier)) . 
      "TEST FAILED: Expected $t_cartesian2, but got $t_cartesian1.\n");
  }

  # manhattan_distance()
  my $t_actual = manhattan_distance(0,8);
  my $t_expected = 4;
  if ($t_cartesian1 == $t_cartesian2) {
    print( sprintf("%-${column_width}s", "manhattan_distance()" . ("." x $dot_multiplier)) . 
      "TEST PASSED: Expected $t_expected, and got $t_actual.\n");
  } else {
    print( sprintf("%-${column_width}s", "manhattan_distance()" . ("." x $dot_multiplier)) . 
      "TEST FAILED: Expected $t_expected, but got $t_actual.\n");
  }

  # manhattan_distance()
  $t_actual = manhattan_distance(0,0);
  $t_expected = 0;
  if ($t_cartesian1 == $t_cartesian2) {
    print( sprintf("%-${column_width}s", "manhattan_distance()" . ("." x $dot_multiplier)) . 
      "TEST PASSED: Expected $t_expected, and got $t_actual.\n");
  } else {
    print( sprintf("%-${column_width}s", "manhattan_distance()" . ("." x $dot_multiplier)) . 
      "TEST FAILED: Expected $t_expected, but got $t_actual.\n");
  }

  # manhattan_distance()
  $t_actual = manhattan_distance(0,1);
  $t_expected = 1;
  if ($t_cartesian1 == $t_cartesian2) {
    print( sprintf("%-${column_width}s", "manhattan_distance()" . ("." x $dot_multiplier)) . 
      "TEST PASSED: Expected $t_expected, and got $t_actual.\n");
  } else {
    print( sprintf("%-${column_width}s", "manhattan_distance()" . ("." x $dot_multiplier)) . 
      "TEST FAILED: Expected $t_expected, but got $t_actual.\n");
  }

  # heuristic1()
  $t_actual = heuristic1(
    [0,1,2,
     3,4,5,
     6,7,8],
    [1,0,3,
     2,5,4,
     8,6,7]
  );
  $t_expected = 9;
  if ($t_actual == $t_expected) {
    print( sprintf("%-${column_width}s", "heuristic1()" . ("." x $dot_multiplier)) . 
      "TEST PASSED: Expected $t_expected, and got $t_actual.\n");
  } else {
    print( sprintf("%-${column_width}s", "heuristic1()" . ("." x $dot_multiplier)) . 
      "TEST FAILED: Expected $t_expected, but got $t_actual.\n");
  }

  # heuristic2()
  $t_actual = heuristic2(
    [0,1,2,
     3,4,5,
     6,7,8],
    [1,0,3,
     2,5,4,
     8,6,7]
  );

  # FIXME - calculate me
  $t_expected = 24;
  if ($t_actual == $t_expected) {
    print( sprintf("%-${column_width}s", "heuristic2()" . ("." x $dot_multiplier)) . 
      "TEST PASSED: Expected $t_expected, and got $t_actual.\n");
  } else {
    print( sprintf("%-${column_width}s", "heuristic2()" . ("." x $dot_multiplier)) . 
      "TEST FAILED: Expected $t_expected, but got $t_actual.\n");
  }

  # get_fringe_states_starter()
  $t_actual = get_fringe_states_starter(

    [1,2,3,
     4,0,5,
     6,7,8],
     my $depth = 2
  );

  print ' EAM $t_actual = ' . Dumper($t_actual) . "\n";

  # FIXME - calculate me
  $t_expected = [
  
    [1,0,3,
     4,2,5,
     6,7,8],
      [0,1,3,
       4,2,5,
       6,7,8],
      [1,3,0,
       4,2,5,
       6,7,8],
      [1,2,3,
       4,0,5,
       6,7,8],


    [1,2,3,
     0,4,5,
     6,7,8],
      [0,2,3,
       1,4,5,
       6,7,8],
      [1,2,3,
       4,0,5,
       6,7,8],
      [1,2,3,
       6,4,5,
       0,7,8],


    [1,2,3,
     4,5,0,
     6,7,8],
      [1,2,0,
       4,5,3,
       6,7,8],
      [1,2,3,
       4,0,5,
       6,7,8],
      [1,2,3,
       4,5,8,
       6,7,0],


    [1,2,3,
     4,7,5,
     6,0,8],
      [1,2,3,
       4,7,5,
       0,6,8],
      [1,2,3,
       4,0,5,
       6,7,8],
      [1,2,3,
       4,7,5,
       6,8,0]
   ];
  
  if (compare_arrays_of_game_states($t_actual, $t_expected)) {
    print( sprintf("%-${column_width}s", "get_fringe_states_starter()" . ("." x $dot_multiplier)) . 
      "TEST PASSED: Expected $t_expected, and got $t_actual.\n");
  } else {
    print( sprintf("%-${column_width}s", "get_fringe_states_starter()" . ("." x $dot_multiplier)) . 
      "TEST FAILED: Expected $t_expected, but got $t_actual.\n");
  }

}

1;
