#!/usr/bin/env perl


#
# How do we get the front item on the queue, without 
# pop()?  Switching to POE::Queue::Array.
#

use List::PriorityQueue;
use Data::Dumper;

my $prio = new List::PriorityQueue;
$prio->insert([4, "foo"], 2);
$prio->insert([4, "bar"], 1);
$prio->insert([4, "baz"], 3);
# my $next = $prio->pop(); # "bar"
# I decided that "foo" isn't as important anymore
$prio->update([56, "foo"], 99);
#

print Dumper($prio);
