package myObject;

require Exporter;

use Heap::Elem;

@ISA = qw(Heap::Elem);

sub new {
    my $self = shift;
    my $value1 = shift;
    my $value2 = shift;

    my $class = ref($self) || $self;

    $self->SUPER::new($class);

    $self->{key} = $value1;
}

sub cmp {
    my $self = shift;
    my $other = shift;

    $self->{key} cmp $other->{key};
}

# other methods for the rest of myObject's functionality
