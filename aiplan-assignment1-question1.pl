#!/usr/bin/env perl
#
#

##################################################################
#
#
# File: aiplan-assignment1-question1.pl
#
#
##################################################################

use POSIX qw(ceil floor);
use Data::Dumper;
use TileGameState;

# Unit tests
require "run-unit-tests.pl";

# Subroutines for managing the tile board game
#   (e.g., finding tile positions, modifying the board)
require "tilegame-helper-subs.pl";

# Global variables for tile game (e.g., board size)
require "tilegame-global-vars.pl";

use strict;

# Question 1
#
# Implement the A* algorithm and the Eight-Puzzle in a programming language of
# your choice.
#
# The most challenging part will probably be the queue of fringe nodes (ordered
# by f-value). For the Eight-Puzzle an array of 32 queues for the different
# possible f-values will do.
#
# (Q: Why 32?) A: Because each of the 8 tiles can branch to - at most -
#   four new states. 8 * 4 = 32
#
#
# A more general solution would be a priority queue implemented as a binary
# tree. Some programming languages have an appropriate data structure in their
# standard library.
#
# The state of the puzzle can be represented by a simple array of numbers that
# lists the tiles in the puzzle row by row. The number zero can be used for the
# empty position. For example, the goal state can be represented by the array
#
#   [0 1 2 3 4 5 6 7 8]
#
# Test your implementation with the initial state [ 1 6 4 8 7 0 3 2 5]. What is
# the optimal solution path length?
#
# Marks available: 2
#

# 0,1,2
# 3,4,5,
# 6,7,8
#
my $global_goal_state = [0, 1, 2, 3, 4, 5, 6, 7, 8];
my $global_start_state;

# Homework goal.  30-step?
$global_start_state = [
  1, 6, 4,
  8, 7, 0,
  3, 2, 5];


# DEBUGGING - easier start state
# 1,0,2
# 3,4,5,
# 6,7,8

# 1-step solution
# $global_start_state  = [
#   0, 1, 2,
#   3, 4, 5,
#   6, 7, 8];

# 2-step solution
# $global_start_state  = [
#   1, 2, 0,
#   3, 4, 5,
#   6, 7, 8];

# 3-step solution
# $global_start_state  = [
#   1, 2, 5,
#   3, 4, 0,
#   6, 7, 8];

# 4-step solution
# $global_start_state  = [
#   1, 2, 5,
#   3, 4, 8,
#   6, 7, 0];

##################################################################
#
#
#
# Heuristics for search algorithm
#
#
#
##################################################################

# Number of misplaced tiles
#
#   h(n) in our cost function defined as
#     f(n) = h(n) + g(n)
sub heuristic1 {
  my ($curr_state, $goal_state) = @_;

  my $misplacement_count = 0;

  for (my $i = 0; $i < @$curr_state; $i++) {
    if ($curr_state->[$i] != $goal_state->[$i]) {
      $misplacement_count++;
    }
  }

  return $misplacement_count;
}

# Sum of each tile's Manhattan-distance to goal
#
#   g(n) in our cost function defined as
#     f(n) = h(n) + g(n)
sub heuristic2 {
  my ($curr_state, $goal_state) = @_;

  my $sum_of_manhattan_distances = 0;

  for (my $i = 0; $i < @$curr_state; $i++) {
    my $find_this = $curr_state->[$i];

    my $found_pos = index_of($find_this, $goal_state);

    if ($found_pos != -1) {
      $sum_of_manhattan_distances += manhattan_distance($i, $found_pos);
    } else {
      die "Couldn't find term $find_this\n";
    }
  }

  return $sum_of_manhattan_distances;
}

##################################################################
#
#
#
# Implementation of A star algorithm
#
#
#
##################################################################

#
# Source: https://en.wikipedia.org/wiki/A*_search_algorithm#Pseudocode
#

# FIXME: In the below [Perl-ified] pseudocode, are the following CLASSES or INSTANCES?
#
#   start
#   neighbor
#   goal
#   came_from
#   current
#
#

# Global table of f-score's and g-score's
my $f_score;
my $g_score;

use Carp;
$SIG{ __DIE__ } = sub { Carp::confess( @_ ) };

sub AStarSearch {
  my ($start, $goal) = @_;

    # The set of nodes already evaluated
    my $closedset;

    # The set of tentative nodes to be evaluated, initially containing the start node
    my $openset;
    $openset->{keyify($start)} = $start;

    # The map of navigated nodes
    my $came_from;
    $came_from->{'s' . keyify($start)} = undef;

    # Cost from start along best known path
    # Q: why is this initialized to 0?  Why not heuristic_cost_estimate()
    # A: The cost from start to start is 0
    $g_score->{keyify($goal)} = 0;

    # Estimated total cost from start to goal through y
    #
    $f_score->{keyify($start)} = $g_score->{keyify($start)} + heuristic_cost_estimate($start, $goal);

    while ((keys %$openset) > 0) {

        # The node in openset having the lowest f_score[} value
        #
        # Structure needed to _BACKTRACE_ from goal to start
        #   came_from -> leaf    = parent1
        #   came_from -> parent1 = parent2
        #   came_from -> parent2 = parent3
        #   came_from -> start   = undef
        #
        my $current = get_best_f_score($openset);

        if ($current ~~ $goal) {
            $came_from->{keyify($goal)} = keyify($current);

            print 'line 219 EAM $openset = ' . Dumper(keys %$openset) . "\n";
            print 'line 217 EAM $came_from = ' . Dumper($came_from) . "\n";
            print 'line 200 EAM reconstruct_path($came_from, $goal) = ' . Dumper(reconstruct_path($came_from, $goal)) . "\n";
            return reconstruct_path($came_from, $goal);
        }

        # remove current from openset
        delete($openset->{keyify($current)});

        # Add current to closedset
        $closedset->{keyify($current)} = $current;

        # Dummy value for Poor Man's scanf debugger
        my $d;

        foreach my $neighbor (@{neighbor_nodes($current)}) {

          print 'line 239 EAM $current = ' . keyify($current) . "\n";
          print 'line 236 EAM $neighbor = ' . keyify($neighbor) . "\n";
          print "\nHit enter to continue\n";
          my $d = <STDIN>;

            if (defined($closedset->{keyify($neighbor)})) {
                next;
            }
            my $tentative_g_score = $g_score->{keyify($current)} + dist_between($current, $neighbor);

            print 'line 248 EAM $tentative_g_score = ' . $tentative_g_score . "\n";
            print 'line 226 EAM $g_score = ' . Dumper($g_score) . "\n";
            print "\nHit enter to continue\n";
            my $d = <STDIN>;

            if ((! defined($openset->{keyify($neighbor)})) or ($tentative_g_score < $g_score->{keyify($neighbor)})) {

                print 'line 236 EAM $neighbor = ' . keyify($neighbor) . "\n";
                print "\nHit enter to continue\n";
                $d = <STDIN>;

                # Is this correct?
                $came_from->{keyify($current)} = keyify($neighbor);

                $g_score->{keyify($neighbor)} = $tentative_g_score;
                $f_score->{keyify($neighbor)} = $g_score->{keyify($neighbor)} + heuristic_cost_estimate($neighbor, $goal);
                if (! defined($openset->{keyify($neighbor)})) {
                    $openset->{keyify($neighbor)} = $neighbor;

                    print 'line 267 EAM Adding $neighbor to openset = ' . keyify($neighbor) . "\n";
                    print "\nHit enter to continue\n";
                    $d = <STDIN>;
                }
            }
        }
    }

    # Failed to find a path.  Can't catch a loop in this algorithm?
    return my $failure = undef;
}

sub keyify {
  my ($arr) = shift;
  return join(",", @$arr);
}

sub get_best_f_score {
  my ($openset) = @_;

  my $best_f_score_key;
  my $best_f_score_val = 2**32;
  foreach my $k (keys %$openset) {
    if ($f_score->{keyify($openset->{$k})} < $best_f_score_val) {
      $best_f_score_val = $f_score->{$k};
      $best_f_score_key = $k;
    }
  }
  return $openset->{$best_f_score_key};
}

# Keep the symbol name (neighbor_nodes) from the Wikipedia pseudocode example
sub neighbor_nodes {
  my ($a) = @_;
  return get_fringe_states($a);
}

# Neighbors are always 1-step away in our toy tile-puzzle
sub dist_between {
  my ($node1, $node2) = @_;
  return 1;
}

# Keep the symbol name (heuristic_cost_estimate) from the Wikipedia pseudocode example
sub heuristic_cost_estimate {
  my ($a, $b) = @_;
  return heuristic2($a, $b);
}

# Keep the symbol name (reconstruct_path) from the Wikipedia pseudocode example
# FIXME: how does this sub work?
sub reconstruct_path {
  my ($came_from, $goal) = @_;

  my $total_path;
  push(@$total_path, keyify($goal));
  foreach my $current (keys %$came_from) {
      push(@$total_path, $current);
  }

  return $total_path
}


# Enumerate fringe states to a depth of n
#
#   . . .
#   . o .
#   . . .
#
#

sub get_fringe_states {
  my ($state) = @_;

  my $fringe = ();

  my $empty_pos = index_of(0, $state);

  # The possible states are moving the open square
  # to a vertically or horizontally adjacent square.
  # Moving open square means swapping the open square
  # with an adjacent occupied square.

  # Calculate the rows and columns
  my ($empty_pos_row, $empty_pos_col) = @{arrayIndex_to_cartesianCoordinates($empty_pos)};

  # Swap empty position with each adjacent tile-occupied positions (if it exists)

  # left
  my $left_pos = cartesianCoordinates_to_arrayIndex($empty_pos_row, $empty_pos_col - 1);

  my $i = 0;

  if (defined($left_pos)) {
    my $new_state1 = swap($state, $empty_pos, $left_pos);
    push(@$fringe, $new_state1);
  }

  # right
  my $right_pos = cartesianCoordinates_to_arrayIndex($empty_pos_row, $empty_pos_col + 1);
  if (defined($right_pos)) {
    my $new_state2 = swap($state, $empty_pos, $right_pos);
    push(@$fringe, $new_state2);
  }

  # above
  my $above_pos = cartesianCoordinates_to_arrayIndex($empty_pos_row - 1, $empty_pos_col);
  if (defined($above_pos)) {
    my $new_state3 = swap($state, $empty_pos, $above_pos);
    push(@$fringe, $new_state3);
  }

  # below
  my $below_pos = cartesianCoordinates_to_arrayIndex($empty_pos_row + 1, $empty_pos_col);
  if (defined($below_pos)) {
    my $new_state4 = swap($state, $empty_pos, $below_pos);
    push(@$fringe, $new_state4);
  }

  return $fringe;
}

my $global_fringe_depth = 2;

# Variant on the above subroutine, but which adds a depth parameter
# to get a _DEEPER_ fringe into the game state tree
sub get_fringe_states2 {
  my ($state, $depth) = @_;

  # If we're at zero depth, we're done.  We can return now.
  if ($depth == 0) {
    return;
  }

  my $fringe = ();

  my $empty_pos = index_of(0, $state);

  # The possible states are moving the open square
  # to a vertically or horizontally adjacent square.
  # Moving open square means swapping the open square
  # with an adjacent occupied square.

  # Calculate the rows and columns
  my ($empty_pos_row, $empty_pos_col) = @{arrayIndex_to_cartesianCoordinates($empty_pos)};

  # Swap empty position with each adjacent tile-occupied positions (if it exists)

  # left
  my $left_pos = cartesianCoordinates_to_arrayIndex($empty_pos_row, $empty_pos_col - 1);

  my $i = 0;

  if (defined($left_pos)) {
    my $new_state1 = swap($state, $empty_pos, $left_pos);
    push(@$fringe, $new_state1);
    my $deeper_fringe1 = get_fringe_states2($new_state1, $depth - 1);
    if (defined($deeper_fringe1)) {
      push(@$fringe, $deeper_fringe1);
    }
  }

  # right
  my $right_pos = cartesianCoordinates_to_arrayIndex($empty_pos_row, $empty_pos_col + 1);
  if (defined($right_pos)) {
    my $new_state2 = swap($state, $empty_pos, $right_pos);
    push(@$fringe, $new_state2);
    my $deeper_fringe2 = get_fringe_states2($new_state2, $depth - 1);
    if (defined($deeper_fringe2)) {
      push(@$fringe, $deeper_fringe2);
    }
  }

  # above
  my $above_pos = cartesianCoordinates_to_arrayIndex($empty_pos_row - 1, $empty_pos_col);
  if (defined($above_pos)) {
    my $new_state3 = swap($state, $empty_pos, $above_pos);
    push(@$fringe, $new_state3);
    my $deeper_fringe3 = get_fringe_states2($new_state3, $depth - 1);
    if (defined($deeper_fringe3)) {
      push(@$fringe, $deeper_fringe3);
    }
  }

  # below
  my $below_pos = cartesianCoordinates_to_arrayIndex($empty_pos_row + 1, $empty_pos_col);
  if (defined($below_pos)) {
    my $new_state4 = swap($state, $empty_pos, $below_pos);
    push(@$fringe, $new_state4);
    my $deeper_fringe4 = get_fringe_states2($new_state4, $depth - 1);
    if (defined($deeper_fringe4)) {
      push(@$fringe, $deeper_fringe4);
    }
  }

  return $fringe;
}

# Outer get_fringe_states routine to kick-off the recursion
sub get_fringe_states_starter {
  return get_fringe_states2(@_);
}


##################################################################
#
#
#
# Main program
#
#
#
##################################################################

run_unit_tests();
# exit;

&AStarSearch($global_start_state, $global_goal_state);
exit;

# DEBUGGING
print "DEBUG: get_fringe_states() ---\n";
foreach my $s (get_fringe_states($global_start_state)) {
  dump_tile_game_state($s);
  print "---\n";
}

#
# vim @d register for debugging perl vars
# vim: let @d = yoprint ' EAM ' . pa . "\n";0wwwwPa = 
#
# vim @l register for adding a line number to a trace
# vim let @l = 0f'aline :let @n = line('.')"np
#
# Combine the two :-)
# vim let @d .= @l
