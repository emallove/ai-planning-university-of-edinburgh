##################################################################
#
#
# File: best-first-search.pl
#
#
##################################################################

sub BestFirstSearch {
    my ($start_state, $goal_state) = @_;

    # Store the steps taken to reach the goal state
    my $game_states_along_pathToGoal;
    push(@$game_states_along_pathToGoal, $start_state);

    # The set of tentative nodes to be evaluated, initially containing the start node
    my $new_fringe;
    my $old_fringe = $start_state;

    # FIXME: is this goal_reached flag needed?
    # Can't we simply break out of the loop when we reach the goal?
    my $goal_reached = undef;
    my $steps_to_goal_count = 0;

    my $lowest_heuristic2_state = $start_state;

    # Descend into game tree while there are still nodes to visit
    while (@$fringe > 0) {

      # FIXME where to put this?
      my $lowest_heuristic2_val = 2**32;

      # Add tile-game states to the fringe
      $new_fringe = get_fringe_states($old_fringe);

      # Select from tiles on the fringe
      my $heuristic2_val;
      foreach my $tile_game_state (@$new_fringe) {

        # Are we at a goal state?
        if (@$tile_game_state ~~ @$goal_state) {

          $goal_reached = 1;
          last;
        }

        $heuristic2_val = heuristic2($tile_game_state, $goal_state);

        # FIXME: are we guaranteed to find a better game state on the fringe?
        # what if we can't?
        # How deep should we be looking - 1-deep, 2-deep, ... n-deep?
        if ($lowest_heuristic2_val > $heuristic2_val) {
          $lowest_heuristic2_val = $heuristic2_val;
          $lowest_heuristic2_state = $tile_game_state;
        }
      }

      # Prepare the least costly state to be expanded at the top of the while loop
      $old_fringe = $lowest_heuristic2_state;

      $steps_to_goal_count++;

      push(@$game_states_along_pathToGoal, $lowest_heuristic2_state);

      # Return, which also breaks us out of the while(1)
      if ($goal_reached) {
        print "219 Goal reached\n\n";
        print "Goal reached in $steps_to_goal_count step(s)!  Tile game states en route to goal state: " . (map { &dump_tile_game_state($_) } @$game_states_along_pathToGoal) . "\n";
        return $game_states_along_pathToGoal;
      }
    }

    # FIXME: Unreachable?
    return undef;
}
 

