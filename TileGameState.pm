
#
# Tile game state
#

package TileGameState;

require Exporter;

sub new {
    my $class = shift;
    my $self = {
        parent => shift,
        children  => shift,
        state_array => shift,
    };
    bless $self, $class;
    return $self;
}

sub toString {
  my $ret = "";

  for (my $i = 0; $i < @{$self->{state_array}}; $i++) {
    if ($i % 3 == 0) {
      $ret .= "\n";
    }
    $ret .= $state->[$i] . " ";
  }

  return $ret;
}

1;
