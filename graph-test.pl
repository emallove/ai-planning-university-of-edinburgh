#!/usr/bin/env perl
#
#

use Data::Dumper;

use Graph;
my $g0 = Graph->new;             # A directed graph.

use Graph::Directed;
my $g1 = Graph::Directed->new;   # A directed graph.

use Graph::Undirected;
my $g2 = Graph::Undirected->new; # An undirected graph.

$g0->add_vertex("a");
$g0->add_vertex("b");
$g0->add_vertex("c");
$g0->add_vertex("d");
$g0->add_vertex("e");

$g0->add_weighted_edge("a", "b", 29);
$g0->add_weighted_edge("a", "c", 12);
$g0->add_weighted_edge("a", "d", 13);
$g0->add_weighted_edge("b", "e", 18);


my @p;
@p = $g0->predecessors("b");
print 'line 30 EAM  @p = ' . Dumper(@p) . "\n";

@p = $g0->predecessors("b");

print 'line 34 EAM  @p = ' . Dumper(@p) . "\n";

exit;

print 'line 29 EAM  $g0 = ' . Dumper($g0) . "\n";

# And many, many more, see below.
